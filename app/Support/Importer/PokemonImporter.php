<?php

namespace App\Support\Importer;

use \App\Models\Pokemon;
use \GuzzleHttp\Client;

class PokemonImporter
{
    /**
     * GuzzleHttp client instance.
     *
     * @var string
     */
    protected $client;

    /**
     * Data from the API.
     *
     * @var string
     */
    protected $apiData;

    /**
     * The url for the api.
     *
     * @var string
     */
    protected $apiUrl = 'http://pokeapi.co/api/v2';

    /**
     * The api endpoint to get data from.
     *
     * @var string
     */
    protected $apiEndpoint = 'pokemon';

    public function __construct()
    {
        $this->client = new Client;
        $this->pokemon = collect([]);
    }

    /**
     * Get a page of pokemon from the api.
     *
     * @param  integer $offset
     * @return object
     */
    protected function getApiPage(int $offset)
    {
        try {
            return json_decode(
                $this->client
                    ->get("{$this->apiUrl}/{$this->apiEndpoint}?limit=40&offset={$offset}")
                    ->getBody()
            );
        } catch (\Exception $e) {
            throw new \App\Exceptions\PokeapiImportException;
        }
    }

    /**
     * Get all pokemon from the API.
     *
     * @return void
     */
    protected function getPokemon()
    {
        $offset = 0;

        // Loop through each api page.
        $response = null;
        do {
            $response = $this->getApiPage($offset);
            // Store pokemon.
            $this->pokemon = $this->pokemon->merge($response->results);

            $offset += 40;
        } while ($response->next);
    }

    /**
     * Run the importer.
     *
     * @return void
     */
    public function run()
    {
        // Get all pokemon from the api.
        $this->getPokemon();

        // Delete pokemon that aren't in the new data.
        Pokemon::whereNotIn('name', $this->pokemon->map->name)->delete();

        // Import each pokemon.
        $this->pokemon->each(function ($pokemon) {
           $pokemon = json_decode($this->client->get($pokemon->url)->getBody());

            $image = $pokemon->sprites->front_default;

            if ($image) {
                $file = \Storage::disk('public')->put(
                    'pokemon/' . basename($image),
                    file_get_contents($image)
                );
            }

            Pokemon::updateOrCreate([
                'id' => $pokemon->id
            ],
            [
                'name'   => $pokemon->name,
                'weight' => $pokemon->weight,
                'height' => $pokemon->height,
                'species' => $pokemon->species->name,
                'abilities' => collect($pokemon->abilities)->map->ability->implode('name', ', '),
                'image'  => $image ? basename($image) : ''
            ]);
        });
    }
}
