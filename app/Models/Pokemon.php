<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'weight', 'height', 'species', 'abilities', 'image'
    ];

    public function getNameAttribute($value)
    {
        return title_case($value);
    }

    public function getAbilitiesAttribute($value)
    {
        return explode(', ', str_replace('-', ' ', $value));
    }

    public function getHeightAttribute($value)
    {
        return ($value / 10) . " m";
    }

    public function getWeightAttribute($value)
    {
        return ($value / 10) . " kg";
    }

    public function getImageUrlAttribute()
    {
        if ($this->image) {
            return url("/storage/pokemon/{$this->image}");
        }

        return url("/images/no-image.png");
    }
}
