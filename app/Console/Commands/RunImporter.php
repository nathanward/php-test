<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RunImporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:pokemon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import pokemon from Pokeapi.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        return (new \App\Support\Importer\PokemonImporter)->run();
    }
}
