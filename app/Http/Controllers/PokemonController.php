<?php

namespace App\Http\Controllers;

use App\Models\Pokemon;

class PokemonController
{
    public function index()
    {
        $pokemon = Pokemon::orderBy('name', 'asc');

        if (request()->has('search')) {
            $pokemon = $pokemon->where(
                'name',
                'like',
                '%' . request()->input('search') . '%'
            );
        }

        $pokemon = $pokemon->paginate(20);

        return view('pokemon.index', compact('pokemon'));
    }

    public function show(Pokemon $pokemon)
    {
        return view('pokemon.show', compact('pokemon'));
    }
}
