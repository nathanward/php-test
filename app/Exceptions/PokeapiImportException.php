<?php

namespace App\Exceptions;

use Exception;

class PokeapiImportException extends Exception
{
    /**
     * The exception description.
     *
     * @var string
     */
    protected $message = 'Error processing Pokeapi import.';
}
