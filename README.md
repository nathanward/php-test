# ukfast/pokédex

## Installation Steps
1. Clone repository
2. `composer install` to install dependencies.
3. `cp .env.example .env` and update database details in new `.env` file.
4. `php artisan key:generate` to create an application key.
5. `php artisan migrate` to generate the database tables.
6. `php artisan storage:link` to setup storage directory.
7. `php artisan import:pokemon` to start pokemon import.

### Additional
A scheduled task has been setup to run the import command at a regular interval, the [Laravel docs](https://laravel.com/docs/master/scheduling) contain information on how to set this up.
