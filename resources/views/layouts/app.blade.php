<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>

<body class="bg-blue-lightest antialiased">
    <div id="app">
        <nav class="bg-blue-light h-16 shadow mb-8 px-6 md:px-0">
            <div class="container mx-auto h-full">
                <div class="flex items-center justify-center h-16">
                    <a href="{{ url('/') }}" class="text-2xl text-blue-darker no-underline flex items-center">
                        <svg class="text-sm w-10 h-10 mr-2 fill-current" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
                            <g>
                                <path d="M500,10.1c-270.5,0-490,219.4-490,489.8c0,270.5,219.5,490,490,490c270.5,0,490-219.5,490-490C990,229.4,770.5,10.1,500,10.1z M500,353.3c81,0,146.8,65.6,146.8,146.6c0,81-65.8,146.8-146.8,146.8c-81,0-146.8-65.8-146.8-146.8C353.2,418.9,419,353.3,500,353.3z M84.7,531.4H290c15.2,102.3,103.5,180.7,210,180.7s194.8-78.4,210-180.7h205.3c-16.2,215.2-196,385.1-415.3,385.1C280.7,916.5,100.8,746.7,84.7,531.4z"/>
                            </g>
                        </svg>
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
