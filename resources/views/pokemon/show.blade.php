@extends('layouts.app')

@section('content')
<div class="md:w-1/2 xl:w-1/3 mx-auto px-6 md:px-0">
    <div>
        <a href="{{ route('pokemon.index') }}" class="no-underline px-4 py-2 bg-blue text-white inline-block">Back to list</a>
    </div>

    <div class="flex flex-wrap my-4 shadow-lg">
        <div class="w-full bg-blue p-6 flex flex-col justify-center">
            <img src="{{ $pokemon->imageUrl }}" class="max-w-full h-auto mx-auto block" />
        </div>

        <div class="w-full bg-blue-light p-6 text-white">
            <h1 class="text-center text-xl md:text-3xl mb-5 uppercase">
                {{ $pokemon->name }}
            </h1>

            <div class="lg:w-2/3 mx-auto text-center flex flex-wrap">
                <div class="w-full mb-4 flex">
                    <div class="w-1/2">
                        <h3 class="uppercase mb-2">Weight</h3>
                        <p class="text-black font-bold">{{ $pokemon->weight }}</p>
                    </div>

                    <div class="w-1/2">
                       <h3 class="uppercase mb-2">Height</h3>
                        <p class="text-black font-bold">{{ $pokemon->height }}</p>
                    </div>
                </div>

                <div class="w-full flex">
                    <div class="w-1/2">
                        <h3 class="uppercase mb-2">Abilites</h3>
                        <ul class="list-reset">
                        @foreach ($pokemon->abilities as $ability)
                            <li class="text-black font-bold mb-1">{{ title_case($ability) }}</li>
                        @endforeach
                        </ul>
                    </div>

                    <div class="w-1/2">
                        <h3 class="uppercase mb-2">Species</h3>
                        <p class="text-black font-bold">{{ title_case($pokemon->species) }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
