@extends('layouts.app')

@section('content')
<div class="md:w-3/4 xl:w-1/2 mx-auto">
    <div class="mb-6">
        <form method="GET">
            <div class="flex flex-wrap shadow">
                <input class="flex-1 py-5 px-3" type="search" name="search" placeholder="Search..." value="{{ request()->input('search') }}" required>
                <div class="w-full md:w-auto flex">
                    <button class="bg-blue text-white px-6 py-3 uppercase font-bold w-1/2">Submit</button>
                    <a href="{{ route('pokemon.index') }}" class="bg-red text-white px-6 py-3 uppercase font-bold no-underline flex items-center w-1/2 justify-center">Reset</a>
                </div>
            </div>
        </form>
    </div>

    <div class="mb-6">
        <div class="-mx-2 flex flex-wrap">
        @forelse ($pokemon as $poke)
            <div class="w-1/2 md:w-1/4 flex flex-col p-2">
                <a href="{{ route('pokemon.show', $poke) }}" class="no-underline flex flex-col h-full shadow">
                    <div class="bg-blue p-3 flex-1 flex flex-col justify-center">
                        <img src="{{ $poke->imageUrl }}" class="max-w-full h-auto mx-auto block" />
                    </div>

                    <div class="bg-blue-light p-3">
                        <h3 class="text-center text-lg text-white">
                            {{ $poke->name }}
                        </h3>
                    </div>
                </a>
            </div>
        @empty
            @if (request()->has('search'))
                <div class="mt-4 px-2 flex-1 justify-center text-center">
                    <h1 class="w-full mb-3 uppercase text-xl">No Results!</h1>
                    <p>Sorry, no pokemon match your search.</p>
                </div>
            @else
                <div class="mt-4 px-2 flex-1 justify-center text-center">
                    <h1 class="w-full mb-3 uppercase text-xl">No Pokemon!</h1>
                    <p>Sorry, there are currently no pokemon.</p>
                </div>
            @endif
        @endforelse
        </div>
    </div>

    <div class="mb-6 flex justify-center">
        {{ $pokemon->links() }}
    </div>
</div>


<div class="w-1/2 mx-auto">
</div>
@endsection
